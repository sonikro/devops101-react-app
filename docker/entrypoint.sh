#!/bin/sh
echo "Running envsubst for process env parameters"
echo "REACT_APP_BACKEND_URL: ${REACT_APP_BACKEND_URL}"
for file in /usr/share/nginx/html/static/js/*.js
do
  echo "Running envsubst for ${file}"
  cat $file | envsubst '$REACT_APP_BACKEND_URL' > "$file-temp"
  cat "$file-temp" > $file
  rm "$file-temp"
done
echo "Serving nginx"
nginx -g 'daemon off;'