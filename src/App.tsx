import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState("");
  const [error, setError] = useState(undefined);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true)
        const response = await fetch(process.env.REACT_APP_BACKEND_URL!)
        const text = await response.text()
        setData(text)
        setError(undefined)
      } catch (error) {
        setError(error)
      } finally {
        setIsLoading(false)
      }
    }
    fetchData()
  }, [])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {isLoading ? <h3>Loading...</h3> : void 0}
        {data ? <h1>Backend data is: {data}</h1> : void 0}
        {error !== undefined ? <h1>Error: {JSON.stringify(error)}</h1> : void 0}
      </header>
    </div>
  );
}

export default App;
