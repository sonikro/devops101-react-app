# devops101-react-app

This app was develop to test GitlabCI features

# Usage

This frontend application needs to run along with [devops101-kotlin-app](https://gitlab.com/sonikro/devops101-kotlin-app) as its backend service

```shellscript
REACT_APP_BACKEND_URL=http://localhost:8080 yarn start
```

# Building the docker image

```shellscript
docker build -t devops101-react-app .
```

# Running the image

```shellscript
docker run -it -p 80:80 -e REACT_APP_BACKEND_URL=http://localhost:8080 devops101-react-app
```
