
FROM node:12 as react-build

ENV REACT_APP_BACKEND_URL '${REACT_APP_BACKEND_URL}'

WORKDIR /app
COPY package.json ./
COPY yarn.lock ./
RUN yarn
COPY . ./
RUN yarn build

FROM nginx:alpine
RUN apk add --no-cache --virtual .gettext gettext
COPY docker/entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build /app/build /usr/share/nginx/html

WORKDIR /etc/nginx
EXPOSE 80
ENTRYPOINT ["entrypoint.sh"]